package main

import (
	"fmt"
	"net/http"
	"sellerapp-crud/db"
	"sellerapp-crud/models"
	"sellerapp-crud/routes"

	"github.com/gorilla/mux"
)

// psSqlCreatedTable @create a table on posgress based on data model
func psSqlCreatedTable() {
	dbInst := db.ConnectPssqlOrm()
	defer dbInst.Close()
	// dbInst.DropTable(models.Todo{})
	dbInst.CreateTable(models.Todo{})
	// dbInst.DropTable(models.User{})
	dbInst.CreateTable(models.User{})
}

func main() {
	port := ":3000"
	psSqlCreatedTable()
	fmt.Println("--> Your App running on add", port)
	http.ListenAndServe(port, routes.PublicRoutes(mux.NewRouter()))
}
