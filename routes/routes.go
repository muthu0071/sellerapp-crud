package routes

import (
	"sellerapp-crud/handler"

	"github.com/gorilla/mux"
)

// RouterInst ...
type RouterInst struct {
	HanderInst handler.IHandler
	TodoInst   handler.ITodoHandlerInst
}

// IRouter
type IRouter interface {
	Routes(r *mux.Router) *mux.Router
}

// PublicRoutes ... @intialize handler Layer @return mux.Router Instance
func PublicRoutes(r *mux.Router) *mux.Router {
	rInst := &RouterInst{
		HanderInst: handler.NewHandlerInst(),
		TodoInst:   handler.NewTodoHandlerInst(),
	}
	return rInst.Routes(r)
}

// Routes @input MuxInstance to handle all the apis
func (rI RouterInst) Routes(r *mux.Router) *mux.Router {
	r.HandleFunc("/api/v1/user", rI.HanderInst.RegisterUser).Methods("POST")
	r.HandleFunc("/api/v1/user", rI.HanderInst.GetAllUser).Methods("GET")

	// @todo Logic
	r.HandleFunc("/api/v1/user/{userId}/todo", rI.TodoInst.CreateTodo).Methods("POST")
	r.HandleFunc("/api/v1/user/{userId}/todo", rI.TodoInst.FindUserTodos).Methods("GET")
	r.HandleFunc("/api/v1/user/{userId}/todo", rI.TodoInst.MarkTodosByUser).Methods("PATCH")
	r.HandleFunc("/api/v1/user/{userId}/todo", rI.TodoInst.DeleteTodosByUser).Methods("DELETE")
	return r
}
