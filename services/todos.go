package services

import (
	"fmt"
	"sellerapp-crud/db"
	"sellerapp-crud/models"
	"time"

	"github.com/jinzhu/gorm"
)

type Todo struct {
}

// ITodo
type ITodo interface {
	CreateTodosByUser(userID uint, todos []models.Todo) (string, error)
	FindTodosByUser(query map[string]interface{}) ([]models.Todo, error)
	MarkTodosByUser(userID uint, todoModel *models.Todo) (string, error)
	DeleteTodosByUser(userID uint, todoModel *models.Todo) (string, error)
}

func NewTodo() ITodo {
	return &Todo{}
}

// CreateTodosByUser ....@input userId, @multiple todo
func (t *Todo) CreateTodosByUser(userID uint, todos []models.Todo) (string, error) {
	dbInst := db.ConnectPssqlOrm()
	defer dbInst.Close()
	var dbResp *gorm.DB
	// @create a multiple todos for the particular user
	for i := 0; i < len(todos); i++ {
		todos[i].CreatedBy = userID
		dbResp = dbInst.Create(&todos[i])
	}

	if dbResp.Error != nil {
		return "", dbResp.Error
	}
	return "", nil
}

// FindTodosByUser find todos for particular Users
func (t *Todo) FindTodosByUser(findQuery map[string]interface{}) (todos []models.Todo, err error) {
	dbInst := db.ConnectPssqlOrm()
	defer dbInst.Close()
	dbResp := dbInst.Not("is_deleted", "yes").Where(findQuery).Find(&todos)
	if dbResp.Error != nil {
		return nil, dbResp.Error
	}
	return todos, nil
}

// MarkTodosByUser ... marked Todo
func (t *Todo) MarkTodosByUser(userID uint, todoModel *models.Todo) (string, error) {
	dbInst := db.ConnectPssqlOrm()
	defer dbInst.Close()
	markedTime := time.Now()
	todoModel.MarkedOn = &markedTime
	dbResp := dbInst.Model(&models.Todo{}).Updates(todoModel, false)
	fmt.Println("--> dbres", dbResp)
	if dbResp.Error != nil {
		return "", dbResp.Error
	}
	return "", nil
}

// DeleteTodosByUser ...
func (t *Todo) DeleteTodosByUser(userID uint, todoModel *models.Todo) (string, error) {
	dbInst := db.ConnectPssqlOrm()
	defer dbInst.Close()
	deletedTime := time.Now()
	todoModel.DeletedOn = &deletedTime
	dbResp := dbInst.Model(&models.Todo{}).Updates(todoModel, true)
	if dbResp.Error != nil {
		return "", dbResp.Error
	}
	return "", nil
}
