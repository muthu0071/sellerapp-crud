package services

import (
	"fmt"
	"sellerapp-crud/db"
	"sellerapp-crud/models"
)

type ServiceInst struct {
}

// IServiceInst @interface for service layer
type IServiceInst interface {
	RegisterUser(user *models.User) (string, error)
	FindUsers(user *models.User) (users []models.User, err error)
}

// NewServiceInst
func NewServiceInst() IServiceInst {
	return &ServiceInst{}
}

// RegisterUser register an user
func (s *ServiceInst) RegisterUser(user *models.User) (string, error) {
	fmt.Println("-> Registeruser")
	dbInst := db.ConnectPssqlOrm()
	defer dbInst.Close()
	// if dbResponse.Error != nil {
	// return "", dbResponse.Error
	// }
	dbCreateResponse := dbInst.Create(user)
	if dbCreateResponse != nil {
		return "", dbCreateResponse.Error
	}
	return "", nil
}

// FindUsers
func (s *ServiceInst) FindUsers(user *models.User) (users []models.User, err error) {
	dbInst := db.ConnectPssqlOrm()
	defer dbInst.Close()
	dbResponse := dbInst.Find(&users)
	if dbResponse.Error != nil {
		err = dbResponse.Error
	}
	return
}
