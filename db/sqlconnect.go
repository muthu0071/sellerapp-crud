package db

import (
	"database/sql"
	"fmt"

	"github.com/jinzhu/gorm"
	_ "github.com/jinzhu/gorm/dialects/postgres"
)

const (
	host     = "localhost"
	port     = 5432
	user     = "muthu"
	password = "muthu"
	dbname   = "calhounio_demo"
)

// ConnectPsSql
func ConnectPsSql() *sql.DB {
	psqlInfo := fmt.Sprintf("host=%s port=%d user=%s password=%s dbname=%s sslmode=disable",
		host, port, user, password, dbname)
	db, err := sql.Open("postgres", psqlInfo)
	if err != nil {
		panic(err)
	}
	e := db.Ping()
	if e != nil {
		panic(err)
	}
	return db
}

// ConnectPssqlOrm ...
func ConnectPssqlOrm() *gorm.DB {
	psqlInfo := fmt.Sprintf("host=%s port=%d user=%s password=%s dbname=%s sslmode=disable",
		host, port, user, password, dbname)
	db, err := gorm.Open("postgres", psqlInfo)
	fmt.Println("--db", db, err)
	if err != nil {
		panic(err)
	}
	return db
}

// CREATE TABLE DEPARTMENT(ID INT PRIMARY KEY      NOT NULL,DEPT CHAR(50) NOT NULL,EMP_ID         INT      NOT NULL);
// psql -h localhost -d calhounio_demo -U muthu -W
