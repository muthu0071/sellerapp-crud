# sellerapp-crud


>logic 

1. A users can create multiple todo lists 

2. A user should be able to create/update/delete/retrieve his todo list 

3. A user should be able to mark his todo as complete w

4. A user should be able to remove his todo 




> Folder Structure
### directory layout

    .
    |__ db                     #postgress db connection                  
    |__ helpers                #helper functions 
    |__ models                 # Data models
    |__ services               #process and query logic for userRegister and marking, deleting todo 
    |__ handler                # process all input from the api 
    |__main.go                 #main thread where router and server instance intiates 
    |__run_tes_coverage.sh    #bash run_tes_coverage.sh to see the code coverage report for entire project
    └── README.md

