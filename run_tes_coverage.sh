echo "total coverage percentage"
go test -coverprofile ./... fmt
rm -rf ...
echo "running test coverage report"
go test -coverpkg= ./... -coverprofile=cover.out
go tool cover -html=cover.out