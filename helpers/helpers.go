package helpers

import (
	"encoding/json"
	"fmt"
	"net/http"
)

// ResponseWith ...send a response with  statuscode
func ResponseWith(w http.ResponseWriter, status int, data ...interface{}) {

	dataB, err := json.Marshal(data)
	// sta, err := strconv.Atoi(status)
	if err != nil {
		w.WriteHeader(status)
		fmt.Fprintf(w, "Invalid Data")
		return
	}
	w.Header().Set("Content-Type", "application/json")
	w.WriteHeader(status)
	w.Write(dataB)
	return
}
