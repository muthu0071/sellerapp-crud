package models

import "time"

// User ...
type User struct {
	ID        uint       `gorm:"primary_key:AUTO_INCREMENT" pg:"id" json:"id"`
	CreatedAt *time.Time `json:"createdAt" pg:"createdAt"`
	UpdatedAt *time.Time `json:"updatedAt" pg:"updatedAt"`
	Name      string     `json:"name" pg:"name"`
	Email     string     `json:"email" pg:"email" gorm:"unique:not null"`
	Mobile    uint       `json:"mobile" pg:"mobile" gorm:"unique:not null"`
}
