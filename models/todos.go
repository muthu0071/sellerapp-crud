package models

import "time"

// Todo  @model for Users Todo
type Todo struct {
	ID              uint       `json:"id" pg:"id" gorm:"primary_key"`
	Name            string     `json:"name,omitempty" pg:"name,omitempty"`
	CreatedBy       uint       `json:"created_by,omitempty" pg:"created_by,omitempty"`
	CreatedAt       *time.Time `json:"createdAt,omitempty"  pg:"createdAt,omitempty"`
	Comments        string     `json:"comments,omitempty" pg:"comments,omitempty"`
	IsMarked        string     `json:"is_marked,omitempty" pg:"is_marked,omitempty"` //yes No
	MarkedComments  string     `json:"markedComments,omitempty" pg:"markedComments,omitempty"`
	MarkedOn        *time.Time `json:"markedOn,omitempty" pg:"markedOn,omitempty"`
	IsDeleted       string     `json:"is_deleted,omitempty" pg:"is_deleted,omitempty"` //yes No
	DeletedComments string     `json:"deletedComments,omitempty" pg:"deletedComments,omitempty"`
	DeletedOn       *time.Time `json:"deletedOn,omitempty" pg:"deletedOn,omitempty"`
	UpdatedAt       *time.Time `json:"updatedAt,omitempty" pg:"updatedAt,omitempty"`
}
