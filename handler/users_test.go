package handler

import (
	"errors"
	"io"
	"net/http"
	"sellerapp-crud/models"
	"testing"
)

type fakeserviceInst struct {
}

func (f *fakeserviceInst) RegisterUser(user *models.User) (string, error) {
	return "", nil
}
func (f *fakeserviceInst) FindUsers(user *models.User) (users []models.User, err error) {
	return nil, nil
}

// FakeNewHandlerInst @handler instance for test case
func FakeNewHandlerInst() IHandler {
	return &HandlerInst{
		&fakeserviceInst{},
	}
}

type fakeResponseWriter struct {
}

func (fr fakeResponseWriter) Write([]byte) (int, error) {
	return 0, nil
}
func (fr fakeResponseWriter) Header() http.Header {

	return nil
}
func (fr fakeResponseWriter) WriteHeader(statusCode int) {

}
func fakeresponseWriter() http.ResponseWriter {
	return fakeResponseWriter{}
}

type fakeioReader struct {
}
type fakeioReadClose struct {
}

func (f fakeioReader) Read(p []byte) (n int, err error) {

	return 0, errors.New("readOperation")
}
func (f fakeioReader) Close() error {
	return nil
}

type fakeIoReadCloser struct {
}

func NewReadCloser() io.ReadCloser {
	return &fakeioReader{}
}

func TestRegisterUser(T *testing.T) {
	fakeRequest := &http.Request{}
	fakeRequest.Body = NewReadCloser()
	fakeWriter := fakeresponseWriter()
	// :w http.ResponseWriter, r *http.Request
	FakeNewHandlerInst().RegisterUser(fakeWriter, fakeRequest)
}
