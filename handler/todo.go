package handler

import (
	"encoding/json"
	"fmt"
	"net/http"
	"sellerapp-crud/helpers"
	"sellerapp-crud/models"
	"sellerapp-crud/services"
	"strconv"

	"github.com/gorilla/mux"
)

// TodoHandlerInst ... handler Instance
type TodoHandlerInst struct {
	Service services.ITodo
}

// ITodoHandlerInst .. handler interface for interacting with gorilla mux
type ITodoHandlerInst interface {
	CreateTodo(w http.ResponseWriter, r *http.Request)
	FindUserTodos(w http.ResponseWriter, r *http.Request)
	DeleteTodosByUser(w http.ResponseWriter, r *http.Request)
	MarkTodosByUser(w http.ResponseWriter, r *http.Request)
}

// NewTodoHandlerInst ...To initiazie TodoHandler Instance to communicating with routes
func NewTodoHandlerInst() ITodoHandlerInst {
	return &TodoHandlerInst{
		Service: services.NewTodo(),
	}
}

// CreateTodo create a todo by particular user
// @sampleInput
// @query params {{userId}}
// @body Params
// [{
// 	"name":"BreakFast",
// 	"comments":"Have Some Fruits"
// },{
// 		"name":"Lunch",
// 	"comments":"Have some vegetables"
// }]
func (t *TodoHandlerInst) CreateTodo(w http.ResponseWriter, r *http.Request) {
	userID := mux.Vars(r)["userId"]
	userIDUint, _ := strconv.Atoi(userID)
	response := ResponseData{}
	todoData := []models.Todo{}
	if err := json.NewDecoder(r.Body).Decode(&todoData); err != nil {
		response.Message = "InValid Data"
		helpers.ResponseWith(w, 400, response)
		return
	}
	errString, err := t.Service.CreateTodosByUser(uint(userIDUint), todoData)
	if err != nil {
		response.Message = err.Error()
		helpers.ResponseWith(w, 500, response)
		return
	}
	if errString != "" {
		response.Message = errString
		helpers.ResponseWith(w, 401, response)
		return
	}
	response.Message = "You have Created Your Todo"
	helpers.ResponseWith(w, 200, response)
	return
}

// FindUserTodos @input @@urlparams {{userId}} @@queryParams {is_marked?}
//query the todo based on who created that Todo and he can see the todo which he has marked
func (t *TodoHandlerInst) FindUserTodos(w http.ResponseWriter, r *http.Request) {
	userID := mux.Vars(r)["userId"]
	fmt.Println("--> userId", userID)
	userIDInt, err := strconv.Atoi(userID)
	response := ResponseData{}
	// @validate the userId it should be a primary key
	if err != nil {
		response.Message = "Please provide the valid userId"
		helpers.ResponseWith(w, 400, response)
		return
	}
	findQuery := map[string]interface{}{
		"created_by": uint(userIDInt),
	}
	isMarked := r.URL.Query().Get("is_marked")
	if isMarked != "" {
		findQuery["is_marked"] = isMarked
	}
	userTodos, err := t.Service.FindTodosByUser(findQuery)
	if err != nil {
		response.Message = err.Error()
		helpers.ResponseWith(w, 500, response)
		return
	}
	response.Data = userTodos
	helpers.ResponseWith(w, 200, response)
	return
}

// DeleteTodosByUser delete a particular todo
// @query params {userId}
// @body params{
// 	"id": 1,
// 	"is_deleted":"yes",
//    "deletedComments":"I dont like to do this Todo"
// }
func (t *TodoHandlerInst) DeleteTodosByUser(w http.ResponseWriter, r *http.Request) {
	userID := mux.Vars(r)["userId"]
	fmt.Println("--> userId", userID)
	userIDUint, _ := strconv.Atoi(userID)
	response := ResponseData{}
	todoData := &models.Todo{}
	if err := json.NewDecoder(r.Body).Decode(&todoData); err != nil {
		response.Message = "InValid Data"
		helpers.ResponseWith(w, 400, response)
		return
	}

	isDeletedInputValidation := todoData.IsDeleted == "yes" || todoData.IsDeleted == "no"
	if !isDeletedInputValidation {
		response.Message = "Please provide the Valid Input for is_marked(yes || no)"
		helpers.ResponseWith(w, 400, response)
		return
	}

	errString, err := t.Service.DeleteTodosByUser(uint(userIDUint), todoData)
	if err != nil {
		response.Message = err.Error()
		helpers.ResponseWith(w, 500, response)
		return
	}
	if errString != "" {
		response.Message = errString
		helpers.ResponseWith(w, 401, response)
		return
	}
	response.Message = "You Have Deleted your Todo SuccessFully"
	helpers.ResponseWith(w, 200, response)
	return
}

// MarkTodosByUser mark a particular todo
// @query params {userId}
// @body params{
// 	"id": 1,
// 	"is_marked":"yes",
//    "markedComments":"I dont like to do this Todo"
// }
func (t *TodoHandlerInst) MarkTodosByUser(w http.ResponseWriter, r *http.Request) {
	userID := mux.Vars(r)["userId"]
	fmt.Println("--> userId", userID)
	userIDUint, _ := strconv.Atoi(userID)
	response := ResponseData{}
	todoData := &models.Todo{}
	if err := json.NewDecoder(r.Body).Decode(&todoData); err != nil {
		response.Message = "InValid Data"
		helpers.ResponseWith(w, 400, response)
		return
	}
	isMarkedInputValidation := todoData.IsMarked == "yes" || todoData.IsMarked == "no"
	if !isMarkedInputValidation {
		response.Message = "Please provide the Valid Input for is_marked(yes || no)"
		helpers.ResponseWith(w, 400, response)
		return
	}
	fmt.Println("--> before service Call")
	errString, err := t.Service.MarkTodosByUser(uint(userIDUint), todoData)
	if err != nil {
		response.Message = err.Error()
		helpers.ResponseWith(w, 500, response)
		return
	}
	if errString != "" {
		response.Message = errString
		helpers.ResponseWith(w, 400, response)
		return
	}
	response.Message = "You Have Marked Your Todo"
	helpers.ResponseWith(w, 200, response)
	return
}
