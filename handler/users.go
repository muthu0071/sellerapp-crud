package handler

import (
	"encoding/json"
	"fmt"
	"net/http"
	"sellerapp-crud/helpers"
	"sellerapp-crud/models"
	"sellerapp-crud/services"
)

// HandlerInst ...
type HandlerInst struct {
	Service services.IServiceInst
}

// IHandler handler interface interacts with router instance
type IHandler interface {
	RegisterUser(w http.ResponseWriter, r *http.Request)
	GetAllUser(w http.ResponseWriter, r *http.Request)
}

type ResponseData struct {
	Message string      `json:"message,omitempty"`
	Data    interface{} `json:"data,omitempty"`
}

func NewHandlerInst() IHandler {
	return &HandlerInst{
		services.NewServiceInst(),
	}
}

// RegisterUser @input user Details
// {
// "name":"KathkiPandi",
// "email":"Muthu!231234@gmail.com",
// "mobile":1987654212
// }
func (h *HandlerInst) RegisterUser(w http.ResponseWriter, r *http.Request) {
	response := ResponseData{}
	usermodel := &models.User{}
	fmt.Println("--> after the struct got intialize")
	if err := json.NewDecoder(r.Body).Decode(&usermodel); err != nil {
		response.Message = "InValid Data"
		fmt.Println("--Err", err.Error())
		helpers.ResponseWith(w, 401, response)
		return
	}

	errString, err := h.Service.RegisterUser(usermodel)
	if err != nil {
		response.Message = err.Error()
		helpers.ResponseWith(w, 500, response)
		return
	}
	if errString != "" {
		response.Message = errString
		helpers.ResponseWith(w, 401, response)
		return
	}
	response.Message = "User Registerd SuccessFully"
	helpers.ResponseWith(w, 200, response)
	return
}

// GetAllUser find all the users from db
func (h *HandlerInst) GetAllUser(w http.ResponseWriter, r *http.Request) {
	users, err := h.Service.FindUsers(&models.User{})
	response := ResponseData{}
	if err != nil {
		response.Message = err.Error()
		helpers.ResponseWith(w, 500, response)
		return
	}
	response.Message = "All users"
	response.Data = users
	helpers.ResponseWith(w, 200, response)
	return
}
